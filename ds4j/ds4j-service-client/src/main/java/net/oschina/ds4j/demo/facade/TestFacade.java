package net.oschina.ds4j.demo.facade;

import net.oschina.ds4j.demo.model.Test;

/**
 * Created by yangwubing on 15/9/5.
 */
public interface TestFacade {

    /**
     *
     * @return
     */
    public Test getById();
}
