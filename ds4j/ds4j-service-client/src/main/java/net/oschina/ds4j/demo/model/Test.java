package net.oschina.ds4j.demo.model;

import java.io.Serializable;

/**
 * Created by yangwubing on 15/9/5.
 */
public class Test implements Serializable{

    private Long id;

    private String name;

    private String remarks;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
