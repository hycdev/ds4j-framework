package net.oschina.ds4j.demo.dao.mybatis;

import net.oschina.ds4j.demo.dao.mybatis.mappers.TestSqlMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

/**
 * Created by yangwubing on 15/9/15.
 */
@ContextConfiguration("classpath:spring/spring-config-dao.xml")
public class TestSqlMapperTest extends AbstractJUnit4SpringContextTests{

    @Autowired
    TestSqlMapper sqlMapper;

    @Test
    public void testGetById(){
        net.oschina.ds4j.demo.model.Test test = sqlMapper.getById(1l);

        System.out.println(test.getId()+", "+test.getName());
    }

}
