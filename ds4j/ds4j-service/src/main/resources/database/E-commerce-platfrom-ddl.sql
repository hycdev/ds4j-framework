-- MySQL Script generated by MySQL Workbench
-- Sat Sep 19 11:29:38 2015
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema demo
-- -----------------------------------------------------
-- 电子商务平台演示案例
DROP SCHEMA IF EXISTS `demo` ;

-- -----------------------------------------------------
-- Schema demo
--
-- 电子商务平台演示案例
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `demo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `demo` ;

-- -----------------------------------------------------
-- Table `demo`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `demo`.`user` ;

CREATE TABLE IF NOT EXISTS `demo`.`user` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `login_name` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NULL,
  `address` VARCHAR(200) NULL,
  `create_time` TIMESTAMP NULL,
  `update_time` TIMESTAMP NULL,
  `phone` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `demo`.`product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `demo`.`product` ;

CREATE TABLE IF NOT EXISTS `demo`.`product` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `class_id` BIGINT(20) NOT NULL,
  `detail` TEXT NULL,
  `create_time` TIMESTAMP NULL,
  `update_time` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `demo`.`product_class`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `demo`.`product_class` ;

CREATE TABLE IF NOT EXISTS `demo`.`product_class` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `create_time` TIMESTAMP NULL,
  `update_time` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `demo`.`file`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `demo`.`file` ;

CREATE TABLE IF NOT EXISTS `demo`.`file` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(45) NOT NULL,
  `size` INT UNSIGNED NOT NULL,
  `create_time` TIMESTAMP NULL,
  `update_time` TIMESTAMP NULL,
  `type` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `demo`.`order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `demo`.`order` ;

CREATE TABLE IF NOT EXISTS `demo`.`order` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(20) NOT NULL,
  `order_time` TIMESTAMP NULL,
  `create_time` TIMESTAMP NULL,
  `update_time` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `demo`.`order_item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `demo`.`order_item` ;

CREATE TABLE IF NOT EXISTS `demo`.`order_item` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `order_id` BIGINT(20) NOT NULL,
  `product_id` BIGINT(20) NOT NULL,
  `price` DOUBLE NOT NULL,
  `amount` VARCHAR(45) NULL,
  `contact` VARCHAR(45) NULL,
  `create_time` TIMESTAMP NULL,
  `update_time` TIMESTAMP NULL,
  `address` VARCHAR(200) NULL,
  `phone` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `demo`.`product_saile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `demo`.`product_saile` ;

CREATE TABLE IF NOT EXISTS `demo`.`product_saile` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` BIGINT(20) NULL,
  `price` DOUBLE NULL,
  `product_sailecol` VARCHAR(45) NULL,
  `amount` FLOAT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `demo`.`product_images`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `demo`.`product_images` ;

CREATE TABLE IF NOT EXISTS `demo`.`product_images` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` BIGINT(20) NULL,
  `file_id` BIGINT(20) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `demo`.`shopping_cart`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `demo`.`shopping_cart` ;

CREATE TABLE IF NOT EXISTS `demo`.`shopping_cart` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '购物车',
  `user_id` BIGINT(20) NOT NULL,
  `create_time` TIMESTAMP NULL,
  `update_time` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `demo`.`shopping_car_item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `demo`.`shopping_car_item` ;

CREATE TABLE IF NOT EXISTS `demo`.`shopping_car_item` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `shopping_cart_id` BIGINT(20) NOT NULL,
  `product_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
