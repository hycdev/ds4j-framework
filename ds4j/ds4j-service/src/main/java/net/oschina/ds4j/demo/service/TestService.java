package net.oschina.ds4j.demo.service;

import net.oschina.ds4j.demo.dao.mybatis.mappers.TestSqlMapper;
import net.oschina.ds4j.demo.model.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by yangwubing on 15/9/5.
 */
@Service
public class TestService {

    @Autowired
    private TestSqlMapper testSqlMapper;

    public Test getById(Long id){
       return testSqlMapper.getById(id);
    }

}
