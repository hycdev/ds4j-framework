package net.oschina.ds4j.demo.dao.mybatis.mappers;

import net.oschina.ds4j.demo.dao.mybatis.common.SqlMapper;
import net.oschina.ds4j.demo.model.Test;

/**
 * Created by yangwubing on 15/9/13.
 */
public interface TestSqlMapper extends SqlMapper {

    public Test getById(Long id);

}
