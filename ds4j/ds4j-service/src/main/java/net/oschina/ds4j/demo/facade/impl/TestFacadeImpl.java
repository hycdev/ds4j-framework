package net.oschina.ds4j.demo.facade.impl;

import com.alibaba.dubbo.config.annotation.Service;
import net.oschina.ds4j.demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import net.oschina.ds4j.demo.model.Test;
import net.oschina.ds4j.demo.facade.TestFacade;

/**
 * Created by yangwubing on 15/9/5.
 */
@Service
public class TestFacadeImpl implements TestFacade{

    @Autowired
    private TestService testService;

    @Override
    public Test getById() {
        return testService.getById(1l);
    }
}
