package net.oschina.ds4j.demo.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import net.oschina.ds4j.demo.facade.TestFacade;
import net.oschina.ds4j.demo.model.Test;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yangwubing on 15/9/4.
 */

@RequestMapping("demo")
@Controller
public class DemoController {


    @Reference
    protected TestFacade testFacade;

    @RequestMapping("test")
    public ModelAndView test(){
        Test test = testFacade.getById();
        return new ModelAndView("/demo/test", "test", test);
    }

}
